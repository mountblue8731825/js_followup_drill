const users = require("./inventory");

const problem1 = () => {
  const interestUsers = {};

  for (let uname in users) {
    let interest = users[uname].interests;
    if (Array.isArray(interest)) {
      const interestarr = interest[0].split(", ");
      for (var i = 0; i < interestarr.length; i++) {
        if (
          interestarr[i] === "Playing Video Games" ||
          interestarr[i] === "Video Games"
        ) {
          interestUsers[uname] = users[uname];
        }
      }
    }
  }
  return interestUsers;
};
console.log(problem1());
