const users = require("./inventory");

const problem2 = () => {
  let citizens = {};
  for (let uname in users) {
    if (users[uname].nationality === "Germany") {
      citizens[uname] = users[uname];
    }
  }
  return citizens;
};

console.log(problem2());
