const users = require("./inventory");

const problem3 = () => {
  let masters = {};
  for (let uname in users) {
    if (users[uname].qualification === "Masters") {
      masters[uname] = users[uname];
    }
  }
  return masters;
};

console.log(problem3());
