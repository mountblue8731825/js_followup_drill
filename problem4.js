const users = require("./inventory");

const problem4 = () => {
  let groups = {};
  const golang = {};
  const javascript = {};
  const python = {};
  for (let uname in users) {
    const detail = users[uname];
    const userdetail = detail.desgination;

    if (userdetail.toLowerCase().includes("javascript")) {
      javascript[uname] = users[uname];
    }
    if (userdetail.toLowerCase().includes("python")) {
      python[uname] = users[uname];
    }
    if (userdetail.toLowerCase().includes("golang")) {
      golang[uname] = users[uname];
    }
    groups.python = python;
    groups.javascript = javascript;
    groups.golang = golang;
    console.log(groups);
  }
};

problem4();
